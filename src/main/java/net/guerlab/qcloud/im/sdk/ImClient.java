package net.guerlab.qcloud.im.sdk;

/**
 * IM客户端
 */
public interface ImClient {

    /**
     * 获取响应对象
     *
     * @param request
     *         请求
     * @param <R>
     *         响应类型
     * @return 响应
     */
    <R extends AbstractImResponse> R getResponse(AbstractImRequest<R> request);

    /**
     * 执行请求
     *
     * @param request
     *         请求
     * @param <R>
     *         响应类型
     * @return 响应
     */
    default <R extends AbstractImResponse> R execute(AbstractImRequest<R> request) {
        R response = getResponse(request);

        if (ImConstants.CODE_SUCCESS != response.getErrorCode()) {
            throw new ImException(
                    "request fail[code=" + response.getErrorCode() + ", msg=" + response.getErrorInfo() + "]");
        }

        return response;
    }
}
