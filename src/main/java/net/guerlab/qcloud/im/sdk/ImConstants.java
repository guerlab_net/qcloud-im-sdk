package net.guerlab.qcloud.im.sdk;

import java.nio.charset.Charset;

/**
 * IM常量
 */
public interface ImConstants {

    /**
     * 默认编码集名称
     */
    String DEFAULT_CHARSET_NAME = "UTF-8";
    /**
     * 默认编码集
     */
    Charset DEFAULT_CHARSET = Charset.forName(ImConstants.DEFAULT_CHARSET_NAME);
    /**
     * 默认数据类型
     */
    String DEFAULT_MEDIA_TYPE = "application/json";

    /**
     * 默认签名方式
     */
    String DEFAULT_SIGN_TYPE = "HmacSHA256";

    /**
     * 默认过期时间
     */
    long DEFAULT_EXPIRE = 86400;

    /**
     * 基础路径
     */
    String BASE_URL = "https://console.tim.qq.com/";

    /**
     * 成功错误码
     */
    int CODE_SUCCESS = 0;

    String PARAMS_SDK_APP_ID = "sdkappid";
    String PARAMS_IDENTIFIER = "identifier";
    String PARAMS_USERSIG = "usersig";
    String PARAMS_RANDOM = "random";
    String PARAMS_CONTENT_TYPE = "contenttype";
    String CONTENT_TYPE_JSON = "json";
}
