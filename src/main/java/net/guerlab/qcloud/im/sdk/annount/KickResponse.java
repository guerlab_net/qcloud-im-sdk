package net.guerlab.qcloud.im.sdk.annount;

import net.guerlab.qcloud.im.sdk.AbstractImResponse;

/**
 * 帐号登录态失效响应
 */
public class KickResponse extends AbstractImResponse {}
