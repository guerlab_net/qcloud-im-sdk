package net.guerlab.qcloud.im.sdk.annount;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.guerlab.qcloud.im.sdk.AbstractImRequest;
import net.guerlab.qcloud.im.sdk.ImException;

import java.util.HashMap;

/**
 * 导入用户请求
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class AccountImportRequest extends AbstractImRequest<AccountImportResponse> {

    /**
     * 账号类型-普通
     */
    public static final Integer TYPE_GENERAL = 0;

    /**
     * 账号类型-机器人
     */
    public static final Integer TYPE_ROBOT = 1;

    /**
     * 用户名，长度不超过32字节
     */
    private String identifier;

    /**
     * 用户昵称
     */
    private String nick;

    /**
     * 用户头像 URL
     */
    private String faceUrl;

    /**
     * 账号类型
     */
    private Integer type;

    @Override
    public String uri() {
        return "v4/im_open_login_svc/account_import";
    }

    @Override
    public byte[] requestBody(ObjectMapper objectMapper) {
        try {
            HashMap<String, Object> jsonObject = new HashMap<>();

            jsonObject.put("Identifier", identifier);
            if (nick != null) {
                jsonObject.put("Nick", nick);
            }
            if (faceUrl != null) {
                jsonObject.put("FaceUrl", faceUrl);
            }
            if (type != null) {
                jsonObject.put("Type", type);
            }

            return objectMapper.writeValueAsBytes(jsonObject);
        } catch (Exception e) {
            throw new ImException(e.getLocalizedMessage(), e);
        }
    }
}
