package net.guerlab.qcloud.im.sdk.autoconfigure;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 启用IM
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
@Import({ IMAutoconfigure.class })
public @interface EnableIM {}
