package net.guerlab.qcloud.im.sdk.msg;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.guerlab.qcloud.im.sdk.AbstractImResponse;

/**
 * 发送消息响应
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SendMsgResponse extends AbstractImResponse {

    /**
     * 消息时间戳，UNIX 时间戳
     */
    @JsonProperty("MsgTime")
    private Integer msgTime;
}
