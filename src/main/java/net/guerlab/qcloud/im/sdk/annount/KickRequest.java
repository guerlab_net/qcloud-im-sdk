package net.guerlab.qcloud.im.sdk.annount;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.guerlab.qcloud.im.sdk.ImException;
import net.guerlab.qcloud.im.sdk.AbstractImRequest;

import java.util.HashMap;

/**
 * 帐号登录态失效请求
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class KickRequest extends AbstractImRequest<KickResponse> {

    /**
     * 用户名，长度不超过32字节
     */
    private String identifier;

    @Override
    public String uri() {
        return "v4/im_open_login_svc/kick";
    }

    @Override
    public byte[] requestBody(ObjectMapper objectMapper) {
        try {
            HashMap<String, Object> jsonObject = new HashMap<>();

            jsonObject.put("Identifier", identifier);

            return objectMapper.writeValueAsBytes(jsonObject);
        } catch (Exception e) {
            throw new ImException(e.getLocalizedMessage(), e);
        }
    }
}
