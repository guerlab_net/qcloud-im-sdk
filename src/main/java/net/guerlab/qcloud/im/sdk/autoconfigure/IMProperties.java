package net.guerlab.qcloud.im.sdk.autoconfigure;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Im配置
 */
@Data
@Component
@ConfigurationProperties(prefix = "im")
public class IMProperties {

    /**
     * sdkAppId
     */
    private Long sdkAppId;

    /**
     * key
     */
    private String key;

    /**
     * identifier
     */
    private String identifier;
}
