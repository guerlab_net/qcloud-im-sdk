package net.guerlab.qcloud.im.sdk.annount;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import net.guerlab.qcloud.im.sdk.AbstractImResponse;

import java.util.List;

/**
 * 删除用户响应
 */
@Data
public class AccountDeleteResponse extends AbstractImResponse {

    /**
     * 单个帐号的结果对象数组
     */
    @JsonProperty("ResultItem")
    private List<ResultItem> ResultItem;

    /**
     * 单个帐号的结果
     */
    @Data
    public static class ResultItem {

        /**
         * 单个帐号的错误码，0表示成功，非0表示失败
         */
        @JsonProperty("ResultCode")
        private Integer resultCode;

        /**
         * 单个帐号删除失败时的错误描述信息
         */
        @JsonProperty("ResultInfo")
        private String resultInfo;

        /**
         * 请求删除的帐号的 UserID
         */
        @JsonProperty("UserID")
        private String userID;
    }
}
