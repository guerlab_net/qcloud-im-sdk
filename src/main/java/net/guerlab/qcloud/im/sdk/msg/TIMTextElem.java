package net.guerlab.qcloud.im.sdk.msg;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 文本消息元素
 */
@Data
public class TIMTextElem implements TIMMsgElement {

    /**
     * 消息内容。当接收方为 iOS 或 Android 后台在线时，作为离线推送的文本展示。
     */
    @JsonProperty("Text")
    private String text;
}
