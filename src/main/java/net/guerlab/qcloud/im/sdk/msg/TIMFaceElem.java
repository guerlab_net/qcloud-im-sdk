package net.guerlab.qcloud.im.sdk.msg;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 表情消息元素
 */
@Data
public class TIMFaceElem implements TIMMsgElement {

    /**
     * 表情索引，用户自定义。
     */
    @JsonProperty("Index")
    private Integer index;

    /**
     * 额外数据。
     */
    @JsonProperty("Data")
    private String data;
}
