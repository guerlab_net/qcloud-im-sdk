package net.guerlab.qcloud.im.sdk.annount;

/**
 * 资料设置项-uint32_t类型
 */
public class IntegerProfileItem extends ProfileItem<Integer> {

    /**
     * 无参数构造
     */
    public IntegerProfileItem() {
    }

    /**
     * 通过指定标签和值来构造一个资料设置项
     *
     * @param tag
     *         标签
     * @param value
     *         值
     */
    public IntegerProfileItem(String tag, Integer value) {
        super(tag, value);
    }
}
