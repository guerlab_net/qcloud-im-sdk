package net.guerlab.qcloud.im.sdk.msg;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 消息内容
 */
@Data
public class MsgBody {

    /**
     * 消息对象类型
     */
    @JsonProperty("MsgType")
    private MsgType msgType;

    /**
     * 消息内容
     */
    @JsonProperty("MsgContent")
    private TIMMsgElement msgContent;
}
