package net.guerlab.qcloud.im.sdk;

/**
 * 请求的处理结果
 */
public enum ActionStatus {
    /**
     * 成功
     */
    OK,

    /**
     * 失败
     */
    FAIL
}
