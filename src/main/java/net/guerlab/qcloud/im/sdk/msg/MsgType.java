package net.guerlab.qcloud.im.sdk.msg;

/**
 * 消息对象类型
 */
public enum MsgType {
    /**
     * 文本消息
     */
    TIMTextElem,

    /**
     * 表情消息
     */
    TIMFaceElem,

    /**
     * 位置消息
     */
    TIMLocationElem,

    /**
     * 自定义消息
     */
    TIMCustomElem,
}
