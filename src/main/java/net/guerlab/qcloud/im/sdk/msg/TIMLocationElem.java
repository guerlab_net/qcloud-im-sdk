package net.guerlab.qcloud.im.sdk.msg;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 地理位置消息元素
 */
@Data
public class TIMLocationElem implements TIMMsgElement {

    /**
     * 地理位置描述信息。
     */
    @JsonProperty("Desc")
    private String desc;

    /**
     * 纬度。
     */
    @JsonProperty("Latitude")
    private double latitude;

    /**
     * 经度。
     */
    @JsonProperty("Longitude")
    private double longitude;
}
