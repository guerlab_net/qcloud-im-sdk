package net.guerlab.qcloud.im.sdk.annount;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.guerlab.qcloud.im.sdk.AbstractImResponse;

import java.util.List;

/**
 * 批量导入用户响应
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class MultiAccountImportResponse extends AbstractImResponse {

    /**
     * 导入失败的帐号列表
     */
    @JsonProperty("FailAccounts")
    private List<String> failAccounts;
}
