package net.guerlab.qcloud.im.sdk.annount;

import net.guerlab.qcloud.im.sdk.AbstractImResponse;

/**
 * 导入用户响应
 */
public class AccountImportResponse extends AbstractImResponse {}
